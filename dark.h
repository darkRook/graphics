#include<graphics.h>
#include<bits/stdc++.h>
#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 75
#define KEY_RIGHT 77
#define KEY_Q 113
#define KEY_E 101
#define KEY_ESC 27
#define arr_size 4
#include<sstream>
#define PI 3.14

using namespace std;


class point{
public:
    double x,y,z,h;
    point(){
        this->x = 0; this->y = 0; this->z = 0; this->h = 1;
    }
    point(double x, double y, double z){
        this->x = x; this->y = y; this->z = z; this->h = 1;
    }
    void print(){
        cout << "{ " << this->x  << " , " << this->y << " , "
         << this->z << " , "  << this->h << " }" << endl;
    }
};

class matrix_packet{

public:
    int N , M ;
    double **index_pointer;
    matrix_packet(){
        this->N = N ; this->M = M ; this->index_pointer = NULL;
    }

    matrix_packet(int N , int M){
        this->index_pointer = new double*[N];
        for(int i=0; i<N; i++){
            this->index_pointer[i] = new double[M];
        }
    }

};


void print_matrix(int N ,int M,double **ptr_mt1){
    for(int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            cout << ptr_mt1[i][j] << " ";
        }cout << endl;
    }
}

void hermite(double x1,double y1,double x2,double y2,double r1,double r2,double r3,double r4)
{
	double x,y,t;

   for(t=0.0;t<=1.0;t+=.001)
	{
		x=(2*t*t*t-3*t*t+1)*x1+(-2*t*t*t+3*t*t)*x2+(t*t*t-2*t*t+t)*r1+(t*t*t-t*t)*r3;
		y=(2*t*t*t-3*t*t+1)*y1+(-2*t*t*t+3*t*t)*y2+(t*t*t-2*t*t+t)*r2+(t*t*t-t*t)*r4;
		putpixel(getmaxx()/2+x,getmaxy()/2-y,GREEN);
	 }

}

matrix_packet matrix_mult(double **mat1, double **mat2)
{
    int i, j, k;
    matrix_packet result = matrix_packet(4,4);
    for (i = 0; i < arr_size; i++)
    {
        for (j = 0; j < arr_size; j++)
        {
            result.index_pointer[i][j] = 0;
            for (k = 0; k < arr_size; k++)
                result.index_pointer[i][j] += mat1[i][k]*mat2[k][j];
        }
    }
    return result;
}

matrix_packet translate_point(double dx, double dy, double dz){
    matrix_packet result = matrix_packet(4,4);

    /**X coordinate**/
    result.index_pointer[0][0] = 1;
    result.index_pointer[1][0] = 0;
    result.index_pointer[2][0] = 0;
    result.index_pointer[3][0] = dx;


    /**Y coordinate**/
    result.index_pointer[0][1] = 0;
    result.index_pointer[1][1] = 1;
    result.index_pointer[2][1] = 0;
    result.index_pointer[3][1] = dy;


    /**Z coordinate**/
    result.index_pointer[0][2] = 0;
    result.index_pointer[1][2] = 0;
    result.index_pointer[2][2] = 1;
    result.index_pointer[3][2] = dz;



    /**h coordinate**/
    result.index_pointer[0][3] = 0;
    result.index_pointer[1][3] = 0;
    result.index_pointer[2][3] = 0;
    result.index_pointer[3][3] = 1;


    return result;

}

matrix_packet scale_point(double sx, double sy , double sz){
        matrix_packet result = matrix_packet(4,4);

    /**X coordinate**/
    result.index_pointer[0][0] = sx;
    result.index_pointer[1][0] = 0;
    result.index_pointer[2][0] = 0;
    result.index_pointer[3][0] = 0;


    /**Y coordinate**/
    result.index_pointer[0][1] = 0;
    result.index_pointer[1][1] = sy;
    result.index_pointer[2][1] = 0;
    result.index_pointer[3][1] = 0;


    /**Z coordinate**/
    result.index_pointer[0][2] = 0;
    result.index_pointer[1][2] = 0;
    result.index_pointer[2][2] = sz;
    result.index_pointer[3][2] = 0;


    /**h coordinate**/
    result.index_pointer[0][3] = 0;
    result.index_pointer[1][3] = 0;
    result.index_pointer[2][3] = 0;
    result.index_pointer[3][3] = 1;

    return result;
}

matrix_packet rotate_point(float angle, float u, float v, float w)
{
    matrix_packet rotationMatrix = matrix_packet(4,4);
    float L = (u*u + v * v + w * w);
    angle = angle * M_PI / 180.0; //converting to radian value
    float u2 = u * u;
    float v2 = v * v;
    float w2 = w * w;

    rotationMatrix.index_pointer[0][0] = (u2 + (v2 + w2) * cos(angle)) / L;
    rotationMatrix.index_pointer[0][1] = (u * v * (1 - cos(angle)) - w * sqrt(L) * sin(angle)) / L;
    rotationMatrix.index_pointer[0][2] = (u * w * (1 - cos(angle)) + v * sqrt(L) * sin(angle)) / L;
    rotationMatrix.index_pointer[0][3] = 0.0;

    rotationMatrix.index_pointer[1][0] = (u * v * (1 - cos(angle)) + w * sqrt(L) * sin(angle)) / L;
    rotationMatrix.index_pointer[1][1] = (v2 + (u2 + w2) * cos(angle)) / L;
    rotationMatrix.index_pointer[1][2] = (v * w * (1 - cos(angle)) - u * sqrt(L) * sin(angle)) / L;
    rotationMatrix.index_pointer[1][3] = 0.0;

    rotationMatrix.index_pointer[2][0] = (u * w * (1 - cos(angle)) - v * sqrt(L) * sin(angle)) / L;
    rotationMatrix.index_pointer[2][1] = (v * w * (1 - cos(angle)) + u * sqrt(L) * sin(angle)) / L;
    rotationMatrix.index_pointer[2][2] = (w2 + (u2 + v2) * cos(angle)) / L;
    rotationMatrix.index_pointer[2][3] = 0.0;

    rotationMatrix.index_pointer[3][0] = 0.0;
    rotationMatrix.index_pointer[3][1] = 0.0;
    rotationMatrix.index_pointer[3][2] = 0.0;
    rotationMatrix.index_pointer[3][3] = 1.0;


    return rotationMatrix;
     //print_matrix(4,rotationMatrix);
}

void put_point(point p, int color){
    putpixel(p.x, p.y, color);
}

void mult_point(point &p , double **mat){

    double curr_point_arr[4] = {0,0,0,0};

    curr_point_arr[0] = (p.x*mat[0][0]) +
     (p.y*mat[1][0]) + (p.z*mat[2][0]) +
     (p.h*mat[3][0]);

    curr_point_arr[1] = (p.x*mat[0][1]) +
     (p.y*mat[1][1]) + (p.z*mat[2][1]) +
     (p.h*mat[3][1]);

    curr_point_arr[2] = (p.x*mat[0][2]) +
     (p.y*mat[1][2]) + (p.z*mat[2][2]) +
     (p.h*mat[3][2]);

    curr_point_arr[3] = (p.x*mat[0][3]) +
     (p.y*mat[1][3]) + (p.z*mat[2][3]) +
     (p.h*mat[3][3]);

     p.x = curr_point_arr[0];
     p.y = curr_point_arr[1];
     p.z = curr_point_arr[2];
     p.h = curr_point_arr[3];

     p.x = p.x / p.h;
     p.y = p.y / p.h;
     p.z = p.z / p.h;
     p.h = p.h / p.h;

}



class cuboid{
public:
    point f1,f2,f3,f4;
    point b1,b2,b3,b4;
    double l,b,h;

    cuboid(){
        this->l = 0; this->b =0; this->h = 0;
    }

    cuboid(double l, double b, double h, double mid_x, double mid_y){
        this->l = l; this->b= b; this->h = h;
        this->f1 = point(mid_x+l/2, mid_y-b/2, h/2);
        this->f2 = point(mid_x-l/2, mid_y-b/2, h/2);
        this->f3 = point(mid_x-l/2, mid_y+b/2, h/2);
        this->f4 = point(mid_x+l/2, mid_y+b/2, h/2);

        //cout << this->f1.x << endl;

        double test_offset = 0;

        this->b1 = point(mid_x+l/2-test_offset, mid_y-b/2-test_offset, -h/2);
        this->b2 = point(mid_x-l/2-test_offset, mid_y-b/2-test_offset, -h/2);
        this->b3 = point(mid_x-l/2-test_offset, mid_y+b/2-test_offset, -h/2);
        this->b4 = point(mid_x+l/2-test_offset, mid_y+b/2-test_offset, -h/2);





    }

    void draw(){
        //cout << this->f1.x << " " << this->f1.y << endl;
        create_line(f1,f2);
        create_line(f2,f3);
        create_line(f3,f4);
        create_line(f4,f1);

        create_line(b1,b2);
        create_line(b2,b3);
        create_line(b3,b4);
        create_line(b4,b1);


        /**connectors to faces**/
        create_line(f1,b1);
        create_line(f2,b2);
        create_line(f3,b3);
        create_line(f4,b4);


    }

    void create_line(point p1, point p2){
        line(p1.x, p1.y, p2.x, p2.y);
    }


};

double to_degree(double radian){
    return (radian*180/PI);
}

class shape_buffer{
public :
    int N; point **arr; point **front_face;
    shape_buffer(){
        this->N = 0; arr = NULL;
    }
    shape_buffer(int N, point **input){
        this->N = N;
        arr = new point*[N];
        for(int i=0; i<N; i++){
            arr[i] =  new point(input[i]->x, input[i]->y, input[i]->z);
            front_face[i] = new point(input[i]->x-100, input[i]->y-100, 0);
        }
    }

    void create_line(point *p1, point *p2){
        line(p1->x, p1->y, p2->x, p2->y);
    }

    void draw(){
        for(int i=0; i<N; i++){
            create_line(arr[i%N], arr[(i+1)%N]);
            create_line(front_face[i%N], front_face[(i+1)%N]);
            create_line(front_face[i], arr[i]);
            arr[i]->print();
        }
    }
};


matrix_packet create_prespective_view(double d, double l, double m, double n){
    matrix_packet result = matrix_packet(4,4);

    //result[0][0] =

}

/**Created By darkRook (founder : deHead)**/




